run: native
		./word_chains.native words.txt cat dog
		./word_chains.native words.txt dog cat
		@echo ---------------------
		./word_chains.native words.txt duck ruby
		./word_chains.native words.txt duck ruby
		@echo ---------------------
		./word_chains.native words.txt rust java
		./word_chains.native words.txt java rust 
		@echo ---------------------
		./word_chains.native words.txt rogue peach
		./word_chains.native words.txt peach rogue
		@echo ---------------------
		./word_chains.native words.txt null funk
		./word_chains.native words.txt funk null

clean:
	rm -f word_chains word_chains.native word_chains.debug

release: word_chains

native: word_chains.native

debug: word_chains.debug

word_chains.native: main.cpp
		g++ -std=c++17 -O2 -march=native -Wall -Wextra -Wpedantic main.cpp -o word_chains.native

word_chains: main.cpp
		g++ -std=c++17 -O2 -Wall -Wextra -Wpedantic main.cpp -o word_chains

word_chains.debug: main.cpp
		g++ -std=c++17 -Og -g -Wall -Wextra -Wpedantic main.cpp -o word_chains.debug
