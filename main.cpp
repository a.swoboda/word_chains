#include <algorithm>
#include <climits>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <string>
#include <vector>
#include <memory>

struct NeighbourHelper {
    NeighbourHelper(std::string value, std::string::size_type ignore) 
        : value(std::move(value))
        , ignore(ignore) {}
    std::string value;
    std::string::size_type ignore;
};

struct NeighbourHelperHash {
    auto operator()(NeighbourHelper const& n) const {
        auto const hasher = std::hash<std::string>{};
        auto seed = hasher(n.value.substr(0, n.ignore));
        seed = hasher(n.value.substr(n.ignore+1)) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        return std::hash<std::string::size_type>{}(n.ignore) + 0x9e3779b9 + (seed<<6) + (seed>>2);
    }
};

struct NeighbourHelperEqualTo {
    bool operator()(NeighbourHelper const& lhs, NeighbourHelper const& rhs) const {
        return lhs.ignore == rhs.ignore 
            and lhs.value.substr(0, lhs.ignore) == rhs.value.substr(0, lhs.ignore)
            and lhs.value.substr(lhs.ignore+1) == rhs.value.substr(lhs.ignore+1);
    }
};

struct NeighbourHelperLess {
    bool operator()(NeighbourHelper const& lhs, NeighbourHelper const& rhs) const {
        if (lhs.ignore < rhs.ignore) { return true; }
        else if (lhs.ignore > rhs.ignore) { return false; }
        // so in the following, lhs.ignore == rhs.ignore holds
        auto const cmp = lhs.value.compare(0, lhs.ignore, rhs.value, 0, lhs.ignore);
        return cmp < 0 
            or (cmp == 0 
                and lhs.value.compare(lhs.ignore+1, lhs.value.size(), rhs.value, lhs.ignore+1, rhs.value.size()) < 0);
    }
};

struct ComputeNeighbours {
    explicit ComputeNeighbours(std::vector<std::string> words) {
        if (! std::is_sorted(begin(words), end(words))) {
            std::sort(begin(words), end(words));
        }
        for (auto&& word : words) {
            for (std::string::size_type n{}, size = word.size(); n != size; ++n) {
                dict.emplace(NeighbourHelper{word, n}, word);
            }
        }
    }

    std::vector<std::string> neighbours(std::string target) const {
        std::vector<std::string> result;
        auto helper = NeighbourHelper{std::move(target), 0};
        for (auto size = helper.value.size(); helper.ignore != size; ++helper.ignore) {
            auto range = dict.equal_range(helper);
            for (; range.first != range.second; ++range.first) {
                if (range.first->second != helper.value) { 
                    result.emplace_back(range.first->second); 
                }
            }
        }
        return result;
    }
private:
    using Dict = std::multimap<NeighbourHelper, std::string, NeighbourHelperLess>;
    // using Dict = std::unordered_multimap<NeighbourHelper, std::string, NeighbourHelperHash, NeighbourHelperEqualTo>;
    Dict dict;
};

struct ComputeNeighboursLazy {
    using Dict = std::vector<std::string>;
    // ATTENTION we assume words of equal length to the searched word here
    explicit ComputeNeighboursLazy(Dict dict) : dict(std::move(dict)) {}

    std::vector<std::string> const& neighbours(std::string const& word) const {
        auto [it, unknown] = lookup.emplace(std::piecewise_construct, std::tie(word), std::tuple<>{});
        if (not unknown) {
            return it->second;
        }
        // else, we need to construct this element
        std::copy_if(begin(dict), end(dict), std::back_inserter(it->second), 
            [&word](auto&& w) {return neighbouring(begin(w), end(w), begin(word));}
        );
        return it->second;
    }
private:
    Dict dict;
    using Lookup = std::unordered_map<std::string, std::vector<std::string>>;
    mutable Lookup lookup;

    template <typename Iterator0, typename Iterator1>
    static bool neighbouring(Iterator0 from0, Iterator0 to0, Iterator1 from1) {
        std::tie(from0, from1) = std::mismatch(from0, to0, from1);
        if (from0 == to0) { return false; }  // they are identical
        return std::equal(++from0, to0, ++from1);
    }
};

template <typename T, typename C=std::less<T>>
struct PriorityQueueSet {
    PriorityQueueSet(C c={}) : lookup(std::move(c)) {}
    
    bool try_pop(T& t) {
        if (lookup.empty()) { return false; }
        auto it = begin(lookup);
        t = *it;
        lookup.erase(it);
        return true;
    }

    template <typename...Ts>
    void emplace(Ts&&...ts) {
        lookup.emplace(std::forward<Ts>(ts)...);
    }

    auto size() const { return lookup.size(); }

    auto empty() const { return lookup.empty(); }

private:
    std::multiset<T, C> lookup;
};

template <typename T, typename C=std::less<T>>
struct PriorityQueueHeap : private C {
    PriorityQueueHeap(C c={}) : C(std::move(c)) {}

    bool try_pop(T& t) {
        if (lookup.empty()) { return false; }
        std::pop_heap(begin(lookup), end(lookup), [this](auto&& lhs, auto&& rhs) { return (*this)(rhs, lhs); });
        t = lookup.back();
        lookup.pop_back();
        return true;
    }

    template <typename...Ts>
    void emplace(Ts&&...ts) {
        lookup.emplace_back(std::forward<Ts>(ts)...);
        std::push_heap(begin(lookup), end(lookup), [this](auto&& lhs, auto&& rhs) { return (*this)(rhs, lhs); });
    }

private:
    std::vector<T> lookup;
};

template <typename T, typename C>
using PriorityQueue = PriorityQueueSet<T, C>;

struct Node {
    explicit Node(std::string word={}, 
        Node const* p=nullptr) 
        : word(std::move(word))
        , predecessor(p) {}

    std::string word;
    Node const* predecessor = nullptr;
};

struct HullNode : Node {
    using Node::Node;
    using length_type = std::uint_fast32_t;
    length_type length = 0;
};

struct AStar {
    using NeighbourLookup = ComputeNeighbours;
    // using NeighbourLookup = ComputeNeighboursLazy;
    AStar(std::string const& start, std::string stop, NeighbourLookup dictionary) 
        : stop(stop)
        , hull(CmpHull{std::move(stop)})
        , lookup{Node{start}}
        , dictionary(std::move(dictionary)) {
        hull.emplace(start, &*lookup.begin());
    }

    enum class ReturnCode {
        SUCCESS = 0,
        FAILURE = 1,
        PENDING = 2
    };

    ReturnCode step() {
        // check if we are already done
        if (0 != lookup.count(Node(stop))) { return ReturnCode::SUCCESS; }
        // pop node from hull
        auto node = HullNode{};
        if (! hull.try_pop(node)) { return ReturnCode::FAILURE; }
        ReturnCode return_code = ReturnCode::PENDING;
        // get all neighbours and add those we haven't visited yet to hull and lookup
        auto&& neighbours = dictionary.neighbours(node.word);
        for (auto&& neighbour_word : neighbours) {
            auto where = lookup.emplace(neighbour_word, node.predecessor);
            if (where.second) {
                if (neighbour_word == stop) { return_code = ReturnCode::SUCCESS; }
                auto hull_element = node;
                hull_element.word = neighbour_word;
                hull_element.predecessor = &*where.first;
                ++hull_element.length;
                hull.emplace(hull_element);
            }
        }
        return return_code;
    }

    Node const* path() const {
        auto const it = lookup.find(Node{stop});  // is_transparent only works from C++20
        return end(lookup) == it ? nullptr : &*it;
    }

private:
    struct CmpHull {
        bool operator()(HullNode const& lhs, HullNode const& rhs) const {
            auto l = comparison_value(lhs);
            auto r = comparison_value(rhs);
            // the second comparison makes this a depth-first search
            return l < r or (l == r and lhs.length > rhs.length);
        }
        std::string const target;
    private:
        HullNode::length_type comparison_value(HullNode const& node) const {
            return node.length + count_mismatches(begin(node.word), end(node.word), begin(target));
        }
        template <typename Iterator0, typename Iterator1>
        HullNode::length_type count_mismatches(Iterator0 from0, Iterator0 to0, Iterator1 from1) const {
            HullNode::length_type result{};
            for (; from0 != to0; ++from0, ++from1) {
                    if (*from0 != *from1) { ++result; }
            }
            return result;
        }
    };
    struct CmpLookup {
        using is_transparent = void;
        bool operator()(Node const& lhs, Node const& rhs) const {
            return lhs.word < rhs.word;
        }
        bool operator()(Node const& lhs, std::string const& rhs) const {
            return lhs.word < rhs;
        }
        bool operator()(std::string const& lhs, Node const& rhs) const {
            return lhs < rhs.word;
        }
    };
    struct HashLookup : std::hash<std::string> {
        std::size_t operator()(Node const& node) const {
            return std::hash<std::string>::operator()(node.word);
        }
    };
    struct EqLookup {
        bool operator()(Node const& lhs, Node const& rhs) const {
            return lhs.word == rhs.word;
        }
    };
    std::string stop;
    PriorityQueue<HullNode, CmpHull> hull;
    using Lookup = std::unordered_set<Node, HashLookup, EqLookup>;
    // using Lookup = std::set<Node, CmpLookup>;
    Lookup lookup;
    NeighbourLookup dictionary;
};

#include <fstream>
#include <iostream>
#include <iterator>

#include <chrono>
#include <cstring>

std::ostream& operator<<(std::ostream& out, Node const* ptr) {
    if (nullptr == ptr) { return out; }
    out << ptr->word;
    if (nullptr == ptr->predecessor) { return out; }
    return out << " <- " << ptr->predecessor;
}

int main(int argc, char* argv[]) {
    if (argc < 4) {
        std::cout << "please supply a dictionary file as first parameter, "
            "and the beginning and end of the word chain "
            "as second and third parameter, respectively";
        return -1;
    }

    auto const len = strlen(argv[2]);
    
    if (strlen(argv[3]) != len) {
        std::cout << "no chain between '" 
                    << argv[2] 
                    << "' and '" 
                    << argv[3] 
                    << "' possible: different string sizes\n";
        return 0;
    }

    auto stream = std::ifstream(argv[1]);
    if (not stream.good()) {
        std::cout << "failed to open file '" << argv[1] << "'\n";
        return -2;
    }

    using Clock = std::chrono::high_resolution_clock;

    auto start_time = Clock::now(); 
    auto words = std::vector<std::string>{};
    std::copy_if(std::istream_iterator<std::string>(stream),
        std::istream_iterator<std::string>{}, 
        std::back_inserter(words), 
        [len](auto&& s) { return s.size() == len; });
    auto dt = Clock::now() - start_time;
    using MilliSeconds = std::chrono::duration<double, std::milli>;
    auto dtms = std::chrono::duration_cast<MilliSeconds>(dt);
    std::cout << "reading in " << words.size() << " words of size " << len 
        << " took " << dtms.count() << " ms.\n";

    start_time = Clock::now(); 
    auto computeNeighbours = AStar::NeighbourLookup(std::move(words));
    dt = Clock::now() - start_time;
    dtms = std::chrono::duration_cast<MilliSeconds>(dt);
    std::cout << "setting up the lookup table took " << dtms.count() << " ms.\n";

    auto algo = AStar(argv[2], argv[3], std::move(computeNeighbours));
    auto rc = AStar::ReturnCode::PENDING;
    start_time = Clock::now();
    auto i=0u;
    for (; rc == AStar::ReturnCode::PENDING; ++i) {
        rc = algo.step();
    }
    dt = Clock::now() - start_time;
    dtms = std::chrono::duration_cast<MilliSeconds>(dt);
    std::cout << i << " iterations took " << dtms.count() << " ms.\n";
    if (AStar::ReturnCode::SUCCESS == rc) {
        std::cout << algo.path() << '\n';
    }
    else {
        std::cout << "no path found.\n";
    }
}
