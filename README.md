# Word Chain

## Task
Given two words of equal length, write a program that can build a chain of words connecting the first to the second. 
Each word in the chain must be in this word list and every step along the chain changes only one letter from the previous word.
For example, given the start word "cat" and the end word "dog", a valid chain would be:
```
"cat", "cot", "cog", "dog".
```
Another example "duck" to "ruby" would have a valid word chain:
```
"duck", "ruck", "rusk", "ruse", "rube", "ruby"
```
If you get your code working, try timing it. 

Does it take less than a second for the above examples? 
And is the timing the same forwards and backwards? 
Does your code find the shortest possible valid word chain?

## Source
https://nwrug.org/quizzes/word-chains-kata

## Details
This uses the A*-algorithm. It's mostly applied to path-finding in 2D, but can be extended to arbitrary topologies in a straight-forward fashion. It is guaranteed to find the optimal solution, if any. The priority queue is set-up to search depth-first by favoring long explored paths over short ones (if the heuristic gives the same expectation for the total length).

## Usage
- call `make` to compile the executable for your native architecture and run some examples
- call `make native`, `make release`, or `make debug`, to build the executables 'word_chains.native', 'word_chains', and 'word_chains.debug', respectively
- call any of those executables with three parameters: given the dicitonary file as first parameter, computes the word chain from the second to the third parameter
